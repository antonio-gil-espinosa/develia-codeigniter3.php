<?php

namespace Develia\CodeIgniter3;

class TableMetadata
{
    public function __construct($table_name)
    {
        $this->table_name = $table_name;
    }

    private $table_name;
    private $primary_key_column_name;

    /**
     * @return mixed
     */
    public function getPrimaryKeyColumnName()
    {
        return $this->primary_key_column_name;
    }

    /**
     * @param mixed $primary_key_column_name
     */
    public function setPrimaryKeyColumnName($primary_key_column_name)
    {
        $this->primary_key_column_name = $primary_key_column_name;
    }

    /**
     * @return mixed
     */
    public function getTableName()
    {
        return $this->table_name;
    }

    /**
     * @param mixed $table_name
     */
    public function setTableName($table_name)
    {
        $this->table_name = $table_name;
    }
}