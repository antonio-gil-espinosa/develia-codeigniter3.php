<?php

namespace Develia\CodeIgniter3;

use CI_DB_query_builder;

/**
 * Class DAO
 *
 * A class representing a Data Access Object for handling database operations.
 * @template T
 */
class DAO
{
    /**
     * @param string |TableMetadata $table_name_or_metadata
     * @param CI_DB_query_builder | null $db
     */
    public function __construct($table_name_or_metadata, $db = null)
    {
        $this->table_metadata = is_string($table_name_or_metadata) ? new TableMetadata($table_name_or_metadata) : $table_name_or_metadata;
        $this->db = $db ? new DB($db) : new DB(get_instance()->db);


    }

    private $table_metadata;


    /**
     * @var CI_DB_query_builder
     */
    private $db;


    /**
     * Delete records from the database.
     *
     * @param array $where The condition to match.
     *
     * @return mixed Returns the number of affected rows on success, FALSE on failure.
     */
    public function delete(array $where)
    {
        return $this->db->delete($this->table_metadata->getTableName(), $where);
    }

    /**
     * Delete records from the database.
     *
     * @param array $where The condition to match.
     *
     * @return mixed Returns the number of affected rows on success, FALSE on failure.
     */
    public function deleteById($id)
    {
        return $this->db->delete($this->table_metadata->getTableName(), [
            $this->_getPrimaryKeyColumnName() => $id
        ]);
    }

    /**
     * Select a single record from the database.
     *
     * @param array $where The condition to match.
     * @param array|null $order_by The column(s) to order by, and the direction (ASC or DESC).
     *
     * @return T|bool The result array or FALSE on failure.
     */
    public function getRow(array $where, array $order_by = null)
    {
        return $this->db->getRow($this->table_metadata->getTableName(), $where, $order_by);
    }

    /**
     * Select a single record from the database using the specified ID.
     *
     * @param mixed $id The ID of the record to retrieve.
     *
     * @return T|bool The result object or FALSE on failure.
     */
    public function getRowById($id)
    {
        return $this->db->getRow($this->table_metadata->getTableName(), [
            $this->_getPrimaryKeyColumnName() => $id
        ]);
    }

    /**
     * Select records from the database.
     *
     * @param array|null $where The condition to match.
     * @param array|null $order_by The column(s) to order by, and the direction (ASC or DESC).
     * @param int|null $limit The maximum number of records to return.
     * @param int|null $offset The number of records to skip before starting to return the records.
     *
     * @return T[]|bool The result array or FALSE on failure.
     */
    public function getRows(array $where = null, array $order_by = null, $limit = null, $offset = null)
    {
        return $this->db->getRows($this->table_metadata->getTableName(), $where, $order_by, $limit, $offset);
    }

    /**
     * Get the value of a specific column from a single record in the database.
     *
     * @param string $column The column to retrieve the value from.
     * @param mixed &$output Reference to the output variable that will hold the column value.
     * @param array|null $where The condition to match.
     *
     * @return bool TRUE if the column value is retrieved successfully, FALSE otherwise.
     */
    public function getScalar($column, &$output, array $where = null)
    {
        return $this->db->getScalar($this->table_metadata->getTableName(), $column, $output, $where);
    }

    /**
     * Insert a new record into the database.
     *
     * @param array $data The data to insert.
     * @param int|null $last_id
     * @return bool Returns TRUE on success, FALSE on failure.
     */
    public function insert(array $data, &$last_id = null)
    {
        return $this->db->insert($this->table_metadata->getTableName(), $data, $last_id);
    }

    /**
     * Update records in the database.
     *
     * @param array $data The data to update.
     * @param array $where The condition to match.
     *
     * @return bool Returns TRUE on success, FALSE on failure.
     */
    public function update(array $data, array $where)
    {
        return $this->db->update($this->table_metadata->getTableName(), $data, $where);
    }

    /**
     * Update records in the database.
     *
     * @param $id
     * @param array $data The data to update.
     * @return bool Returns TRUE on success, FALSE on failure.
     */
    public function updateById($id, array $data)
    {
        return $this->db->update($this->table_metadata->getTableName(), $data, [
            $this->_getPrimaryKeyColumnName() => $id
        ]);
    }

    /**
     * @return mixed
     */
    private function _getPrimaryKeyColumnName()
    {
        $primary_key_column_name = $this->table_metadata->getPrimaryKeyColumnName();

        if (!$primary_key_column_name) {
            $primary_key_column_name = $this->db->getPrimaryKeyColumnName($this->table_metadata->getTableName());
            $this->table_metadata->setPrimaryKeyColumnName($primary_key_column_name);
        }
        return $primary_key_column_name;
    }

}