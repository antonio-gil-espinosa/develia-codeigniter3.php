<?php

namespace Develia\CodeIgniter3;

/**
 * Clase DB para operaciones de base de datos.
 *
 * @method static mixed delete(string $table, array $where)
 * @method static array|bool getRows(string $table, array $where = null, array $order_by = null, int $limit = null, int $offset = null)
 * @method static mixed|bool getField(string $table, string $column, mixed &$output, array $where = null)
 * @method static array|bool getRow(string $table, array $where, array $order_by = null)
 * @method static bool insert(string $table, array $data, int|null &$last_id = null)
 * @method static bool update(string $table, array $data, array $where)
 */
class DB
{

    public function __construct(\CI_DB_query_builder $db)
    {
        $this->db = $db;
    }

    /**
     * @var DB
     */
    private static $default_instance;

    /**
     * @var \CI_DB_query_builder
     */
    private $db;

    public static function __callStatic($name, $arguments)
    {
        return call_user_func_array([self::getDefaultInstance(), $name], $arguments);
    }

    /**
     * Get the default instance of the DB class.
     *
     * @return DB The default instance of the DB class.
     */
    public static function getDefaultInstance()
    {
        if (is_null(self::$default_instance)) {
            self::$default_instance = new DB(get_instance()->db);
        }
        return self::$default_instance;

    }

    /**
     * Delete records from the database.
     *
     * @param array $where The condition to match.
     *
     * @return mixed Returns the number of affected rows on success, FALSE on failure.
     */
    public function delete($table, array $where)
    {
        $this->db->where($where);
        return $this->db->delete($table);
    }

    /**
     * Get the value of a specific column from a table in the database.
     *
     * @param string $table The name of the table.
     * @param string $column The name of the column to retrieve the value from.
     * @param mixed &$output A reference variable to store the value of the column.
     * @param array|null $where The condition to match.
     *
     * @return bool Returns TRUE if the value is successfully retrieved and stored in $output, or FALSE if not found or an error occurred.
     */
    public function getScalar($table, $column, &$output, array $where = null)
    {
        if ($where) {
            $this->db->where($where);
        }

        // Select the specific column
        $this->db->select($column);

        // Limit the result to 1
        $this->db->limit(1);

        $query = $this->db->get($table);

        if ($query && $query->num_rows() > 0) {
            $result = $query->row_array();
            $output = $result[$column];
            return true;
        } else {
            return false;
        }
    }

    public function getPrimaryKeyColumnName($table)
    {
        $database = $this->db->database;
        $this->db->select('COLUMN_NAME')
                 ->from('information_schema.COLUMNS')
                 ->where('TABLE_SCHEMA', $database)
                 ->where('TABLE_NAME', $table)
                 ->where('COLUMN_KEY', 'PRI');
        return $this->db->get()->row()->COLUMN_NAME;
    }

    /**
     * Select a single record from the database.
     *
     * @param array $where The condition to match.
     * @param array|null $order_by The column(s) to order by, and the direction (ASC or DESC).
     *
     * @return mixed|bool The result array or FALSE on failure.
     */
    public function getRow($table, array $where, array $order_by = null)
    {
        if ($where) {
            $this->db->where($where);
        }

        if ($order_by) {
            foreach ($order_by as $column => $direction) {
                $this->db->order_by($column, $direction);
            }
        }

        // Limit the result to 1
        $this->db->limit(1);

        $query = $this->db->get($table);

        if ($query && $query->num_rows() > 0) {
            return $query->row();
        } else {
            return false;
        }
    }

    /**
     * Select records from the database.
     *
     * @param array|null $where The condition to match.
     * @param array|null $order_by The column(s) to order by, and the direction (ASC or DESC).
     * @param int|null $limit The maximum number of records to return.
     * @param int|null $offset The number of records to skip before starting to return the records.
     *
     * @return array|bool The result array or FALSE on failure.
     */
    public function getRows($table, array $where = null, array $order_by = null, $limit = null, $offset = null)
    {
        if ($where) {
            $this->db->where($where);
        }

        if ($order_by) {
            foreach ($order_by as $column => $direction) {
                $this->db->order_by($column, $direction);
            }
        }

        if (!is_null($limit)) {
            if (!is_null($offset)) {
                $this->db->limit($limit, $offset);
            } else {
                $this->db->limit($limit);
            }
        }

        $query = $this->db->get($table);

        if ($query) {
            return $query->result();
        } else {
            return false;
        }
    }

    /**
     * Insert a new record into the database.
     *
     * @param array $data The data to insert.
     * @param int|null $last_id
     * @return bool Returns TRUE on success, FALSE on failure.
     */
    public function insert($table, array $data, &$last_id = null)
    {
        $success = $this->db->insert($table, $data);
        if ($success)
            $last_id = $this->db->insert_id();

        return $success;
    }

    /**
     * Update records in the database.
     *
     * @param array $data The data to update.
     * @param array $where The condition to match.
     *
     * @return bool Returns TRUE on success, FALSE on failure.
     */
    public function update($table, array $data, array $where)
    {
        $this->db->where($where);
        return $this->db->update($table, $data);
    }
}