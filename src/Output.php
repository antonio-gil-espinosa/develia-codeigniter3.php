<?php

namespace Develia\CodeIgniter3;

/**
 * Class Output
 *
 * The Output class provides methods for generating HTTP responses.
 */
class Output
{
    protected function __construct() {}

    /**
     * Redirects the user to a specified URL with an optional HTTP status code.
     *
     * @param string $url The URL to redirect the user to.
     * @param int $statusCode The HTTP status code to use for the redirect. Defaults to 302.
     *
     * @return never-returns
     */
    public static function redirect($url, $statusCode = 302)
    {
        $CI =& get_instance();
        $CI->load->helper('url');
        redirect($url, $statusCode);
        exit();
    }

    /**
     * Outputs the given object as a JSON response.
     *
     * @param mixed $object The object to be converted to JSON.
     * @param int $return_code The HTTP status code to be set in the response. Default is 200.
     * @return never-returns
     */
    public static function json($object, $return_code = 200)
    {
        $CI =& get_instance();
        $CI->output
            ->set_status_header($return_code)
            ->set_content_type('application/json')
            ->set_output(json_encode($object))
            ->_display();

        exit();
    }

    /**
     * Sets the HTTP status code for the response.
     *
     * @param int $code The HTTP status code to be set.
     * @return never-returns
     */
    public static function statusCode($code)
    {
        $CI =& get_instance();
        $CI->output->set_status_header($code);
        $CI->output->_display();

        exit();
    }

    /**
     * Outputs the given data as a response.
     *
     * @param mixed $data The data to be outputted.
     * @param string $content_type The content type of the response. Default is "application/octet-stream".
     * @param int $return_code The HTTP status code to be set in the response. Default is 200.
     * @return never-returns
     */
    public static function content($data, $content_type = "application/octet-stream", $return_code = 200)
    {
        $CI =& get_instance();
        $CI->output->set_status_header($return_code)
                   ->set_content_type($content_type)
                   ->set_output($data)
                   ->_display();

        exit();
    }


    /**
     * Sets HTTP response content type to "text/html" and provides the specified HTML content.
     *
     * @param string $html The HTML content to be displayed in the response body.
     * @param int $return_code The HTTP status code to be returned in the response. Default is 200 (OK).
     *
     * @return never-returns
     */
    public static function html($html, $return_code = 200)
    {
        self::content($html, "text/html", $return_code);
    }


    /**
     * Sets HTTP response content type to "text/csv" and provides the specified data as CSV file for download.
     *
     * @param array $data The data to be converted to CSV format and displayed in the response body.
     * @param string $filename The name of the CSV file that will be downloaded by the client.
     * @param int $return_code The HTTP status code to be returned in the response. Default is 200 (OK).
     *
     * @return never-returns
     */
    public static function csv($data, $filename, $return_code = 200)
    {
        $CI =& get_instance();

        $CI->output->set_status_header($return_code)
                   ->set_content_type('text/csv')
                   ->set_header("Cache-Control: no-store, no-cache")
                   ->set_header('Content-Disposition: attachment; filename="' . $filename . '"');

        $fp = null;
        try {
            $fp = fopen('php://memory', 'w');
            foreach ($data as $row) {
                fputcsv($fp, array_values($row));
            }

            fseek($fp, 0);
            fpassthru($fp);
        } finally {
            if ($fp) {
                fclose($fp);
            }
        }

        exit();
    }

    /**
     * Sets the HTTP response content type based on the file extension and provides the specified file as a download.
     *
     * @param string $filePath The path to the file to be downloaded.
     * @param string $downloadFileName The name to be used when saving the file on the client-side. If empty, the original file name will be used.
     * @param int $return_code The HTTP status code to be returned in the response. Default is 200 (OK).
     *
     * @return never-returns
     */
    public static function file($filePath, $downloadFileName = "", $return_code = 200)
    {
        $CI =& get_instance();

        if (!file_exists($filePath)) {
            $CI->output->set_status_header(404);
            exit();
        }

        $CI->output
            ->set_status_header($return_code)
            ->set_content_type(MimeType::fromExtension(pathinfo($filePath, PATHINFO_EXTENSION)))
            ->set_header('Content-Disposition: attachment; filename="' . ($downloadFileName ?: basename($filePath)) . '"')
            ->set_output(file_get_contents($filePath))
            ->_display();

        exit();
    }
}